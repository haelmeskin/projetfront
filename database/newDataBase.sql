PGDMP         8                y            postgres    11.10    12.3 �               0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            	           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            
           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false                       1262    13012    postgres    DATABASE     �   CREATE DATABASE postgres WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'French_France.1252' LC_CTYPE = 'French_France.1252';
    DROP DATABASE postgres;
                postgres    false                       0    0    DATABASE postgres    COMMENT     N   COMMENT ON DATABASE postgres IS 'default administrative connection database';
                   postgres    false    3083                        3079    16384 	   adminpack 	   EXTENSION     A   CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;
    DROP EXTENSION adminpack;
                   false                       0    0    EXTENSION adminpack    COMMENT     M   COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';
                        false    1            �            1259    32956    activite    TABLE     �   CREATE TABLE public.activite (
    activite_id bigint NOT NULL,
    id_court character varying(255),
    id_long character varying(255),
    libelle character varying(255)
);
    DROP TABLE public.activite;
       public            postgres    false            �            1259    32954    activite_activite_id_seq    SEQUENCE     �   CREATE SEQUENCE public.activite_activite_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.activite_activite_id_seq;
       public          postgres    false    233                       0    0    activite_activite_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.activite_activite_id_seq OWNED BY public.activite.activite_id;
          public          postgres    false    232            �            1259    17513 	   categorie    TABLE     �   CREATE TABLE public.categorie (
    categorie_id bigint NOT NULL,
    libelle character varying(255),
    nom character varying(255),
    projet_id bigint
);
    DROP TABLE public.categorie;
       public            postgres    false            �            1259    17511    categorie_categorie_id_seq    SEQUENCE     �   CREATE SEQUENCE public.categorie_categorie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.categorie_categorie_id_seq;
       public          postgres    false    198                       0    0    categorie_categorie_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.categorie_categorie_id_seq OWNED BY public.categorie.categorie_id;
          public          postgres    false    197            �            1259    17524    client    TABLE       CREATE TABLE public.client (
    client_id bigint NOT NULL,
    adresse character varying(255),
    ident_tva character varying(255),
    nom character varying(255),
    ref_client character varying(255),
    siret character varying(255),
    telephone character varying(255)
);
    DROP TABLE public.client;
       public            postgres    false            �            1259    17522    client_client_id_seq    SEQUENCE     }   CREATE SEQUENCE public.client_client_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.client_client_id_seq;
       public          postgres    false    200                       0    0    client_client_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.client_client_id_seq OWNED BY public.client.client_id;
          public          postgres    false    199            �            1259    17535 
   complexite    TABLE     �   CREATE TABLE public.complexite (
    complexite_id bigint NOT NULL,
    libelle character varying(255),
    nom character varying(255),
    valeur double precision NOT NULL,
    domaine_id bigint
);
    DROP TABLE public.complexite;
       public            postgres    false            �            1259    17533    complexite_complexite_id_seq    SEQUENCE     �   CREATE SEQUENCE public.complexite_complexite_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.complexite_complexite_id_seq;
       public          postgres    false    202                       0    0    complexite_complexite_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.complexite_complexite_id_seq OWNED BY public.complexite.complexite_id;
          public          postgres    false    201            �            1259    32877    delivery    TABLE     �   CREATE TABLE public.delivery (
    delivery_id bigint NOT NULL,
    libelle character varying(255),
    nom character varying(255),
    projet_id bigint
);
    DROP TABLE public.delivery;
       public            postgres    false            �            1259    32875    delivery_delivery_id_seq    SEQUENCE     �   CREATE SEQUENCE public.delivery_delivery_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.delivery_delivery_id_seq;
       public          postgres    false    231                       0    0    delivery_delivery_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.delivery_delivery_id_seq OWNED BY public.delivery.delivery_id;
          public          postgres    false    230            �            1259    24578    devis    TABLE     �   CREATE TABLE public.devis (
    devis_id bigint NOT NULL,
    libelle character varying(255),
    date_creation timestamp without time zone,
    date_validation timestamp without time zone,
    devis_projet_id bigint NOT NULL
);
    DROP TABLE public.devis;
       public            postgres    false            �            1259    17557    devis_complexite    TABLE     �  CREATE TABLE public.devis_complexite (
    devis_complexite_id bigint NOT NULL,
    date timestamp without time zone,
    libelle character varying(255),
    nom character varying(255),
    valeur double precision NOT NULL,
    domaine_id bigint,
    devis_complexite_item_id bigint,
    categorie_description character varying(255),
    categorie_id bigint,
    categorie_name character varying(255),
    complexite_id bigint NOT NULL,
    complexite_libelle character varying(255),
    complexite_nom character varying(255),
    complexite_valeur double precision NOT NULL,
    domaine_desciption character varying(255),
    domaine_name character varying(255)
);
 $   DROP TABLE public.devis_complexite;
       public            postgres    false            �            1259    17555 (   devis_complexite_devis_complexite_id_seq    SEQUENCE     �   CREATE SEQUENCE public.devis_complexite_devis_complexite_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.devis_complexite_devis_complexite_id_seq;
       public          postgres    false    204                       0    0 (   devis_complexite_devis_complexite_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.devis_complexite_devis_complexite_id_seq OWNED BY public.devis_complexite.devis_complexite_id;
          public          postgres    false    203            �            1259    24586    devis_complexite_item    TABLE     v   CREATE TABLE public.devis_complexite_item (
    devis_complexite_item_id bigint NOT NULL,
    devis_item_id bigint
);
 )   DROP TABLE public.devis_complexite_item;
       public            postgres    false            �            1259    24584 2   devis_complexite_item_devis_complexite_item_id_seq    SEQUENCE     �   CREATE SEQUENCE public.devis_complexite_item_devis_complexite_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 I   DROP SEQUENCE public.devis_complexite_item_devis_complexite_item_id_seq;
       public          postgres    false    217                       0    0 2   devis_complexite_item_devis_complexite_item_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.devis_complexite_item_devis_complexite_item_id_seq OWNED BY public.devis_complexite_item.devis_complexite_item_id;
          public          postgres    false    216            �            1259    24576    devis_devis_id_seq    SEQUENCE     {   CREATE SEQUENCE public.devis_devis_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.devis_devis_id_seq;
       public          postgres    false    215                       0    0    devis_devis_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.devis_devis_id_seq OWNED BY public.devis.devis_id;
          public          postgres    false    214            �            1259    24594    devis_echancier    TABLE     {  CREATE TABLE public.devis_echancier (
    devis_echancier_id bigint NOT NULL,
    date timestamp without time zone,
    delivery_id bigint,
    etape_echancier_id bigint,
    pourcentage integer NOT NULL,
    devis_echancier_item_id bigint,
    delivery_dom character varying(255),
    delivery_libelle character varying(255),
    etape_echancier_etape character varying(255)
);
 #   DROP TABLE public.devis_echancier;
       public            postgres    false            �            1259    24592 &   devis_echancier_devis_echancier_id_seq    SEQUENCE     �   CREATE SEQUENCE public.devis_echancier_devis_echancier_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 =   DROP SEQUENCE public.devis_echancier_devis_echancier_id_seq;
       public          postgres    false    219                       0    0 &   devis_echancier_devis_echancier_id_seq    SEQUENCE OWNED BY     q   ALTER SEQUENCE public.devis_echancier_devis_echancier_id_seq OWNED BY public.devis_echancier.devis_echancier_id;
          public          postgres    false    218            �            1259    24602    devis_echancier_item    TABLE     t   CREATE TABLE public.devis_echancier_item (
    devis_echancier_item_id bigint NOT NULL,
    devis_item_id bigint
);
 (   DROP TABLE public.devis_echancier_item;
       public            postgres    false            �            1259    24600 0   devis_echancier_item_devis_echancier_item_id_seq    SEQUENCE     �   CREATE SEQUENCE public.devis_echancier_item_devis_echancier_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 G   DROP SEQUENCE public.devis_echancier_item_devis_echancier_item_id_seq;
       public          postgres    false    221                       0    0 0   devis_echancier_item_devis_echancier_item_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.devis_echancier_item_devis_echancier_item_id_seq OWNED BY public.devis_echancier_item.devis_echancier_item_id;
          public          postgres    false    220            �            1259    24610 
   devis_item    TABLE     �   CREATE TABLE public.devis_item (
    devis_item_id bigint NOT NULL,
    evolution character varying(255),
    fonction character varying(255),
    devis_id bigint
);
    DROP TABLE public.devis_item;
       public            postgres    false            �            1259    24608    devis_item_devis_item_id_seq    SEQUENCE     �   CREATE SEQUENCE public.devis_item_devis_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.devis_item_devis_item_id_seq;
       public          postgres    false    223                       0    0    devis_item_devis_item_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.devis_item_devis_item_id_seq OWNED BY public.devis_item.devis_item_id;
          public          postgres    false    222            �            1259    32809    devis_projet    TABLE     x  CREATE TABLE public.devis_projet (
    devis_projet_id bigint NOT NULL,
    categorie_id bigint NOT NULL,
    categorie_libelle character varying(255),
    categorie_nom character varying(255),
    client_id bigint,
    client_nom character varying(255),
    commentaire character varying(255),
    nom_projet character varying(255),
    num_contrat character varying(255)
);
     DROP TABLE public.devis_projet;
       public            postgres    false            �            1259    32807     devis_projet_devis_projet_id_seq    SEQUENCE     �   CREATE SEQUENCE public.devis_projet_devis_projet_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.devis_projet_devis_projet_id_seq;
       public          postgres    false    229                       0    0     devis_projet_devis_projet_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.devis_projet_devis_projet_id_seq OWNED BY public.devis_projet.devis_projet_id;
          public          postgres    false    228            �            1259    17579    devis_ratio    TABLE     �  CREATE TABLE public.devis_ratio (
    devis_ratio_id bigint NOT NULL,
    date timestamp without time zone,
    ordre_affichage integer NOT NULL,
    pourcentage double precision NOT NULL,
    activite_id bigint NOT NULL,
    delivery_id bigint NOT NULL,
    devis_retio_item_id bigint,
    activite_id_court character varying(255),
    activite_id_long character varying(255),
    activite_libelle character varying(255),
    delivery_libelle character varying(255),
    delivery_nom character varying(255)
);
    DROP TABLE public.devis_ratio;
       public            postgres    false            �            1259    17577    devis_ratio_devis_ratio_id_seq    SEQUENCE     �   CREATE SEQUENCE public.devis_ratio_devis_ratio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.devis_ratio_devis_ratio_id_seq;
       public          postgres    false    206                       0    0    devis_ratio_devis_ratio_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.devis_ratio_devis_ratio_id_seq OWNED BY public.devis_ratio.devis_ratio_id;
          public          postgres    false    205            �            1259    24621    devis_ratio_item    TABLE     l   CREATE TABLE public.devis_ratio_item (
    devis_ratio_item_id bigint NOT NULL,
    devis_item_id bigint
);
 $   DROP TABLE public.devis_ratio_item;
       public            postgres    false            �            1259    24619 (   devis_ratio_item_devis_ratio_item_id_seq    SEQUENCE     �   CREATE SEQUENCE public.devis_ratio_item_devis_ratio_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.devis_ratio_item_devis_ratio_item_id_seq;
       public          postgres    false    225                       0    0 (   devis_ratio_item_devis_ratio_item_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.devis_ratio_item_devis_ratio_item_id_seq OWNED BY public.devis_ratio_item.devis_ratio_item_id;
          public          postgres    false    224            �            1259    24629    devis_tj_item    TABLE     f   CREATE TABLE public.devis_tj_item (
    devis_tj_item_id bigint NOT NULL,
    devis_item_id bigint
);
 !   DROP TABLE public.devis_tj_item;
       public            postgres    false            �            1259    24627 "   devis_tj_item_devis_tj_item_id_seq    SEQUENCE     �   CREATE SEQUENCE public.devis_tj_item_devis_tj_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.devis_tj_item_devis_tj_item_id_seq;
       public          postgres    false    227                       0    0 "   devis_tj_item_devis_tj_item_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.devis_tj_item_devis_tj_item_id_seq OWNED BY public.devis_tj_item.devis_tj_item_id;
          public          postgres    false    226            �            1259    17587    devistj    TABLE     ?  CREATE TABLE public.devistj (
    devis_tj_id bigint NOT NULL,
    cout double precision NOT NULL,
    date timestamp without time zone,
    client_id bigint NOT NULL,
    profil_id bigint NOT NULL,
    devis_tj_item_id bigint,
    client_adresse character varying(255),
    client_com character varying(255),
    client_ident_tva character varying(255),
    client_ref_client character varying(255),
    client_siret character varying(255),
    client_telephone character varying(255),
    prodil_cout double precision NOT NULL,
    prodil_libelle character varying(255)
);
    DROP TABLE public.devistj;
       public            postgres    false            �            1259    17585    devistj_devis_tj_id_seq    SEQUENCE     �   CREATE SEQUENCE public.devistj_devis_tj_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.devistj_devis_tj_id_seq;
       public          postgres    false    208                       0    0    devistj_devis_tj_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.devistj_devis_tj_id_seq OWNED BY public.devistj.devis_tj_id;
          public          postgres    false    207            �            1259    17595    domaine    TABLE     �   CREATE TABLE public.domaine (
    domaine_id bigint NOT NULL,
    libelle character varying(255),
    nom character varying(255),
    categorie_id bigint
);
    DROP TABLE public.domaine;
       public            postgres    false            �            1259    17593    domaine_domaine_id_seq    SEQUENCE        CREATE SEQUENCE public.domaine_domaine_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.domaine_domaine_id_seq;
       public          postgres    false    210                       0    0    domaine_domaine_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.domaine_domaine_id_seq OWNED BY public.domaine.domaine_id;
          public          postgres    false    209            �            1259    17604 	   echancier    TABLE     �   CREATE TABLE public.echancier (
    delivery_id bigint NOT NULL,
    etape_echancier_id bigint NOT NULL,
    pourcentage integer NOT NULL
);
    DROP TABLE public.echancier;
       public            postgres    false            �            1259    33030    etape_echancier    TABLE     r   CREATE TABLE public.etape_echancier (
    etape_echancier_id bigint NOT NULL,
    etape character varying(255)
);
 #   DROP TABLE public.etape_echancier;
       public            postgres    false            �            1259    33028 &   etape_echancier_etape_echancier_id_seq    SEQUENCE     �   CREATE SEQUENCE public.etape_echancier_etape_echancier_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 =   DROP SEQUENCE public.etape_echancier_etape_echancier_id_seq;
       public          postgres    false    239                       0    0 &   etape_echancier_etape_echancier_id_seq    SEQUENCE OWNED BY     q   ALTER SEQUENCE public.etape_echancier_etape_echancier_id_seq OWNED BY public.etape_echancier.etape_echancier_id;
          public          postgres    false    238            �            1259    33002    profil    TABLE     b   CREATE TABLE public.profil (
    profil_id bigint NOT NULL,
    libelle character varying(255)
);
    DROP TABLE public.profil;
       public            postgres    false            �            1259    33000    profil_profil_id_seq    SEQUENCE     }   CREATE SEQUENCE public.profil_profil_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.profil_profil_id_seq;
       public          postgres    false    236                        0    0    profil_profil_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.profil_profil_id_seq OWNED BY public.profil.profil_id;
          public          postgres    false    235            �            1259    17627    projet    TABLE     �   CREATE TABLE public.projet (
    projet_id bigint NOT NULL,
    commentaire character varying(255),
    nom_projet character varying(255),
    num_contrat character varying(255),
    client_id bigint
);
    DROP TABLE public.projet;
       public            postgres    false            �            1259    17625    projet_projet_id_seq    SEQUENCE     }   CREATE SEQUENCE public.projet_projet_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.projet_projet_id_seq;
       public          postgres    false    213            !           0    0    projet_projet_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.projet_projet_id_seq OWNED BY public.projet.projet_id;
          public          postgres    false    212            �            1259    32965    ratio    TABLE     �   CREATE TABLE public.ratio (
    activite_id bigint NOT NULL,
    delivery_id bigint NOT NULL,
    ordre_affichage integer NOT NULL,
    pourcentage double precision NOT NULL
);
    DROP TABLE public.ratio;
       public            postgres    false            �            1259    33013    tj    TABLE        CREATE TABLE public.tj (
    delivery_id bigint NOT NULL,
    profil_id bigint NOT NULL,
    cout double precision NOT NULL
);
    DROP TABLE public.tj;
       public            postgres    false                       2604    32959    activite activite_id    DEFAULT     |   ALTER TABLE ONLY public.activite ALTER COLUMN activite_id SET DEFAULT nextval('public.activite_activite_id_seq'::regclass);
 C   ALTER TABLE public.activite ALTER COLUMN activite_id DROP DEFAULT;
       public          postgres    false    232    233    233            	           2604    17516    categorie categorie_id    DEFAULT     �   ALTER TABLE ONLY public.categorie ALTER COLUMN categorie_id SET DEFAULT nextval('public.categorie_categorie_id_seq'::regclass);
 E   ALTER TABLE public.categorie ALTER COLUMN categorie_id DROP DEFAULT;
       public          postgres    false    197    198    198            
           2604    17527    client client_id    DEFAULT     t   ALTER TABLE ONLY public.client ALTER COLUMN client_id SET DEFAULT nextval('public.client_client_id_seq'::regclass);
 ?   ALTER TABLE public.client ALTER COLUMN client_id DROP DEFAULT;
       public          postgres    false    200    199    200                       2604    17538    complexite complexite_id    DEFAULT     �   ALTER TABLE ONLY public.complexite ALTER COLUMN complexite_id SET DEFAULT nextval('public.complexite_complexite_id_seq'::regclass);
 G   ALTER TABLE public.complexite ALTER COLUMN complexite_id DROP DEFAULT;
       public          postgres    false    201    202    202                       2604    32880    delivery delivery_id    DEFAULT     |   ALTER TABLE ONLY public.delivery ALTER COLUMN delivery_id SET DEFAULT nextval('public.delivery_delivery_id_seq'::regclass);
 C   ALTER TABLE public.delivery ALTER COLUMN delivery_id DROP DEFAULT;
       public          postgres    false    230    231    231                       2604    24581    devis devis_id    DEFAULT     p   ALTER TABLE ONLY public.devis ALTER COLUMN devis_id SET DEFAULT nextval('public.devis_devis_id_seq'::regclass);
 =   ALTER TABLE public.devis ALTER COLUMN devis_id DROP DEFAULT;
       public          postgres    false    215    214    215                       2604    17560 $   devis_complexite devis_complexite_id    DEFAULT     �   ALTER TABLE ONLY public.devis_complexite ALTER COLUMN devis_complexite_id SET DEFAULT nextval('public.devis_complexite_devis_complexite_id_seq'::regclass);
 S   ALTER TABLE public.devis_complexite ALTER COLUMN devis_complexite_id DROP DEFAULT;
       public          postgres    false    204    203    204                       2604    24589 .   devis_complexite_item devis_complexite_item_id    DEFAULT     �   ALTER TABLE ONLY public.devis_complexite_item ALTER COLUMN devis_complexite_item_id SET DEFAULT nextval('public.devis_complexite_item_devis_complexite_item_id_seq'::regclass);
 ]   ALTER TABLE public.devis_complexite_item ALTER COLUMN devis_complexite_item_id DROP DEFAULT;
       public          postgres    false    216    217    217                       2604    24597 "   devis_echancier devis_echancier_id    DEFAULT     �   ALTER TABLE ONLY public.devis_echancier ALTER COLUMN devis_echancier_id SET DEFAULT nextval('public.devis_echancier_devis_echancier_id_seq'::regclass);
 Q   ALTER TABLE public.devis_echancier ALTER COLUMN devis_echancier_id DROP DEFAULT;
       public          postgres    false    218    219    219                       2604    24605 ,   devis_echancier_item devis_echancier_item_id    DEFAULT     �   ALTER TABLE ONLY public.devis_echancier_item ALTER COLUMN devis_echancier_item_id SET DEFAULT nextval('public.devis_echancier_item_devis_echancier_item_id_seq'::regclass);
 [   ALTER TABLE public.devis_echancier_item ALTER COLUMN devis_echancier_item_id DROP DEFAULT;
       public          postgres    false    220    221    221                       2604    24613    devis_item devis_item_id    DEFAULT     �   ALTER TABLE ONLY public.devis_item ALTER COLUMN devis_item_id SET DEFAULT nextval('public.devis_item_devis_item_id_seq'::regclass);
 G   ALTER TABLE public.devis_item ALTER COLUMN devis_item_id DROP DEFAULT;
       public          postgres    false    223    222    223                       2604    32812    devis_projet devis_projet_id    DEFAULT     �   ALTER TABLE ONLY public.devis_projet ALTER COLUMN devis_projet_id SET DEFAULT nextval('public.devis_projet_devis_projet_id_seq'::regclass);
 K   ALTER TABLE public.devis_projet ALTER COLUMN devis_projet_id DROP DEFAULT;
       public          postgres    false    229    228    229                       2604    17582    devis_ratio devis_ratio_id    DEFAULT     �   ALTER TABLE ONLY public.devis_ratio ALTER COLUMN devis_ratio_id SET DEFAULT nextval('public.devis_ratio_devis_ratio_id_seq'::regclass);
 I   ALTER TABLE public.devis_ratio ALTER COLUMN devis_ratio_id DROP DEFAULT;
       public          postgres    false    206    205    206                       2604    24624 $   devis_ratio_item devis_ratio_item_id    DEFAULT     �   ALTER TABLE ONLY public.devis_ratio_item ALTER COLUMN devis_ratio_item_id SET DEFAULT nextval('public.devis_ratio_item_devis_ratio_item_id_seq'::regclass);
 S   ALTER TABLE public.devis_ratio_item ALTER COLUMN devis_ratio_item_id DROP DEFAULT;
       public          postgres    false    225    224    225                       2604    24632    devis_tj_item devis_tj_item_id    DEFAULT     �   ALTER TABLE ONLY public.devis_tj_item ALTER COLUMN devis_tj_item_id SET DEFAULT nextval('public.devis_tj_item_devis_tj_item_id_seq'::regclass);
 M   ALTER TABLE public.devis_tj_item ALTER COLUMN devis_tj_item_id DROP DEFAULT;
       public          postgres    false    226    227    227                       2604    17590    devistj devis_tj_id    DEFAULT     z   ALTER TABLE ONLY public.devistj ALTER COLUMN devis_tj_id SET DEFAULT nextval('public.devistj_devis_tj_id_seq'::regclass);
 B   ALTER TABLE public.devistj ALTER COLUMN devis_tj_id DROP DEFAULT;
       public          postgres    false    207    208    208                       2604    17598    domaine domaine_id    DEFAULT     x   ALTER TABLE ONLY public.domaine ALTER COLUMN domaine_id SET DEFAULT nextval('public.domaine_domaine_id_seq'::regclass);
 A   ALTER TABLE public.domaine ALTER COLUMN domaine_id DROP DEFAULT;
       public          postgres    false    210    209    210                       2604    33033 "   etape_echancier etape_echancier_id    DEFAULT     �   ALTER TABLE ONLY public.etape_echancier ALTER COLUMN etape_echancier_id SET DEFAULT nextval('public.etape_echancier_etape_echancier_id_seq'::regclass);
 Q   ALTER TABLE public.etape_echancier ALTER COLUMN etape_echancier_id DROP DEFAULT;
       public          postgres    false    239    238    239                       2604    33005    profil profil_id    DEFAULT     t   ALTER TABLE ONLY public.profil ALTER COLUMN profil_id SET DEFAULT nextval('public.profil_profil_id_seq'::regclass);
 ?   ALTER TABLE public.profil ALTER COLUMN profil_id DROP DEFAULT;
       public          postgres    false    236    235    236                       2604    17630    projet projet_id    DEFAULT     t   ALTER TABLE ONLY public.projet ALTER COLUMN projet_id SET DEFAULT nextval('public.projet_projet_id_seq'::regclass);
 ?   ALTER TABLE public.projet ALTER COLUMN projet_id DROP DEFAULT;
       public          postgres    false    212    213    213            �          0    32956    activite 
   TABLE DATA           K   COPY public.activite (activite_id, id_court, id_long, libelle) FROM stdin;
    public          postgres    false    233   ��       �          0    17513 	   categorie 
   TABLE DATA           J   COPY public.categorie (categorie_id, libelle, nom, projet_id) FROM stdin;
    public          postgres    false    198   �       �          0    17524    client 
   TABLE DATA           b   COPY public.client (client_id, adresse, ident_tva, nom, ref_client, siret, telephone) FROM stdin;
    public          postgres    false    200   9�       �          0    17535 
   complexite 
   TABLE DATA           U   COPY public.complexite (complexite_id, libelle, nom, valeur, domaine_id) FROM stdin;
    public          postgres    false    202   "�       �          0    32877    delivery 
   TABLE DATA           H   COPY public.delivery (delivery_id, libelle, nom, projet_id) FROM stdin;
    public          postgres    false    231   n�       �          0    24578    devis 
   TABLE DATA           c   COPY public.devis (devis_id, libelle, date_creation, date_validation, devis_projet_id) FROM stdin;
    public          postgres    false    215   ��       �          0    17557    devis_complexite 
   TABLE DATA             COPY public.devis_complexite (devis_complexite_id, date, libelle, nom, valeur, domaine_id, devis_complexite_item_id, categorie_description, categorie_id, categorie_name, complexite_id, complexite_libelle, complexite_nom, complexite_valeur, domaine_desciption, domaine_name) FROM stdin;
    public          postgres    false    204   y�       �          0    24586    devis_complexite_item 
   TABLE DATA           X   COPY public.devis_complexite_item (devis_complexite_item_id, devis_item_id) FROM stdin;
    public          postgres    false    217   ��       �          0    24594    devis_echancier 
   TABLE DATA           �   COPY public.devis_echancier (devis_echancier_id, date, delivery_id, etape_echancier_id, pourcentage, devis_echancier_item_id, delivery_dom, delivery_libelle, etape_echancier_etape) FROM stdin;
    public          postgres    false    219   ��       �          0    24602    devis_echancier_item 
   TABLE DATA           V   COPY public.devis_echancier_item (devis_echancier_item_id, devis_item_id) FROM stdin;
    public          postgres    false    221   ��       �          0    24610 
   devis_item 
   TABLE DATA           R   COPY public.devis_item (devis_item_id, evolution, fonction, devis_id) FROM stdin;
    public          postgres    false    223   ��       �          0    32809    devis_projet 
   TABLE DATA           �   COPY public.devis_projet (devis_projet_id, categorie_id, categorie_libelle, categorie_nom, client_id, client_nom, commentaire, nom_projet, num_contrat) FROM stdin;
    public          postgres    false    229   
�       �          0    17579    devis_ratio 
   TABLE DATA           �   COPY public.devis_ratio (devis_ratio_id, date, ordre_affichage, pourcentage, activite_id, delivery_id, devis_retio_item_id, activite_id_court, activite_id_long, activite_libelle, delivery_libelle, delivery_nom) FROM stdin;
    public          postgres    false    206   @�       �          0    24621    devis_ratio_item 
   TABLE DATA           N   COPY public.devis_ratio_item (devis_ratio_item_id, devis_item_id) FROM stdin;
    public          postgres    false    225   ]�       �          0    24629    devis_tj_item 
   TABLE DATA           H   COPY public.devis_tj_item (devis_tj_item_id, devis_item_id) FROM stdin;
    public          postgres    false    227   z�       �          0    17587    devistj 
   TABLE DATA           �   COPY public.devistj (devis_tj_id, cout, date, client_id, profil_id, devis_tj_item_id, client_adresse, client_com, client_ident_tva, client_ref_client, client_siret, client_telephone, prodil_cout, prodil_libelle) FROM stdin;
    public          postgres    false    208   ��       �          0    17595    domaine 
   TABLE DATA           I   COPY public.domaine (domaine_id, libelle, nom, categorie_id) FROM stdin;
    public          postgres    false    210   ��       �          0    17604 	   echancier 
   TABLE DATA           Q   COPY public.echancier (delivery_id, etape_echancier_id, pourcentage) FROM stdin;
    public          postgres    false    211   ��                 0    33030    etape_echancier 
   TABLE DATA           D   COPY public.etape_echancier (etape_echancier_id, etape) FROM stdin;
    public          postgres    false    239   ��                 0    33002    profil 
   TABLE DATA           4   COPY public.profil (profil_id, libelle) FROM stdin;
    public          postgres    false    236   �       �          0    17627    projet 
   TABLE DATA           \   COPY public.projet (projet_id, commentaire, nom_projet, num_contrat, client_id) FROM stdin;
    public          postgres    false    213   P�                  0    32965    ratio 
   TABLE DATA           W   COPY public.ratio (activite_id, delivery_id, ordre_affichage, pourcentage) FROM stdin;
    public          postgres    false    234   ��                 0    33013    tj 
   TABLE DATA           :   COPY public.tj (delivery_id, profil_id, cout) FROM stdin;
    public          postgres    false    237   ��       "           0    0    activite_activite_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.activite_activite_id_seq', 16, true);
          public          postgres    false    232            #           0    0    categorie_categorie_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.categorie_categorie_id_seq', 7, true);
          public          postgres    false    197            $           0    0    client_client_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.client_client_id_seq', 3, true);
          public          postgres    false    199            %           0    0    complexite_complexite_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.complexite_complexite_id_seq', 34, true);
          public          postgres    false    201            &           0    0    delivery_delivery_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.delivery_delivery_id_seq', 4, true);
          public          postgres    false    230            '           0    0 (   devis_complexite_devis_complexite_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.devis_complexite_devis_complexite_id_seq', 1, false);
          public          postgres    false    203            (           0    0 2   devis_complexite_item_devis_complexite_item_id_seq    SEQUENCE SET     a   SELECT pg_catalog.setval('public.devis_complexite_item_devis_complexite_item_id_seq', 1, false);
          public          postgres    false    216            )           0    0    devis_devis_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.devis_devis_id_seq', 13, true);
          public          postgres    false    214            *           0    0 &   devis_echancier_devis_echancier_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.devis_echancier_devis_echancier_id_seq', 1, false);
          public          postgres    false    218            +           0    0 0   devis_echancier_item_devis_echancier_item_id_seq    SEQUENCE SET     _   SELECT pg_catalog.setval('public.devis_echancier_item_devis_echancier_item_id_seq', 1, false);
          public          postgres    false    220            ,           0    0    devis_item_devis_item_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.devis_item_devis_item_id_seq', 1, false);
          public          postgres    false    222            -           0    0     devis_projet_devis_projet_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.devis_projet_devis_projet_id_seq', 64, true);
          public          postgres    false    228            .           0    0    devis_ratio_devis_ratio_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.devis_ratio_devis_ratio_id_seq', 1, false);
          public          postgres    false    205            /           0    0 (   devis_ratio_item_devis_ratio_item_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.devis_ratio_item_devis_ratio_item_id_seq', 1, false);
          public          postgres    false    224            0           0    0 "   devis_tj_item_devis_tj_item_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.devis_tj_item_devis_tj_item_id_seq', 1, false);
          public          postgres    false    226            1           0    0    devistj_devis_tj_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.devistj_devis_tj_id_seq', 1, false);
          public          postgres    false    207            2           0    0    domaine_domaine_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.domaine_domaine_id_seq', 7, true);
          public          postgres    false    209            3           0    0 &   etape_echancier_etape_echancier_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.etape_echancier_etape_echancier_id_seq', 10, true);
          public          postgres    false    238            4           0    0    profil_profil_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.profil_profil_id_seq', 3, true);
          public          postgres    false    235            5           0    0    projet_projet_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.projet_projet_id_seq', 8, true);
          public          postgres    false    212            B           2606    32964    activite activite_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.activite
    ADD CONSTRAINT activite_pkey PRIMARY KEY (activite_id);
 @   ALTER TABLE ONLY public.activite DROP CONSTRAINT activite_pkey;
       public            postgres    false    233                       2606    17521    categorie categorie_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.categorie
    ADD CONSTRAINT categorie_pkey PRIMARY KEY (categorie_id);
 B   ALTER TABLE ONLY public.categorie DROP CONSTRAINT categorie_pkey;
       public            postgres    false    198                        2606    17532    client client_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.client
    ADD CONSTRAINT client_pkey PRIMARY KEY (client_id);
 <   ALTER TABLE ONLY public.client DROP CONSTRAINT client_pkey;
       public            postgres    false    200            "           2606    17543    complexite complexite_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY public.complexite
    ADD CONSTRAINT complexite_pkey PRIMARY KEY (complexite_id);
 D   ALTER TABLE ONLY public.complexite DROP CONSTRAINT complexite_pkey;
       public            postgres    false    202            @           2606    32885    delivery delivery_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.delivery
    ADD CONSTRAINT delivery_pkey PRIMARY KEY (delivery_id);
 @   ALTER TABLE ONLY public.delivery DROP CONSTRAINT delivery_pkey;
       public            postgres    false    231            2           2606    24591 0   devis_complexite_item devis_complexite_item_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.devis_complexite_item
    ADD CONSTRAINT devis_complexite_item_pkey PRIMARY KEY (devis_complexite_item_id);
 Z   ALTER TABLE ONLY public.devis_complexite_item DROP CONSTRAINT devis_complexite_item_pkey;
       public            postgres    false    217            $           2606    17565 &   devis_complexite devis_complexite_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY public.devis_complexite
    ADD CONSTRAINT devis_complexite_pkey PRIMARY KEY (devis_complexite_id);
 P   ALTER TABLE ONLY public.devis_complexite DROP CONSTRAINT devis_complexite_pkey;
       public            postgres    false    204            6           2606    24607 .   devis_echancier_item devis_echancier_item_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.devis_echancier_item
    ADD CONSTRAINT devis_echancier_item_pkey PRIMARY KEY (devis_echancier_item_id);
 X   ALTER TABLE ONLY public.devis_echancier_item DROP CONSTRAINT devis_echancier_item_pkey;
       public            postgres    false    221            4           2606    24599 $   devis_echancier devis_echancier_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.devis_echancier
    ADD CONSTRAINT devis_echancier_pkey PRIMARY KEY (devis_echancier_id);
 N   ALTER TABLE ONLY public.devis_echancier DROP CONSTRAINT devis_echancier_pkey;
       public            postgres    false    219            8           2606    24618    devis_item devis_item_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY public.devis_item
    ADD CONSTRAINT devis_item_pkey PRIMARY KEY (devis_item_id);
 D   ALTER TABLE ONLY public.devis_item DROP CONSTRAINT devis_item_pkey;
       public            postgres    false    223            0           2606    24583    devis devis_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.devis
    ADD CONSTRAINT devis_pkey PRIMARY KEY (devis_id);
 :   ALTER TABLE ONLY public.devis DROP CONSTRAINT devis_pkey;
       public            postgres    false    215            >           2606    32817    devis_projet devis_projet_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.devis_projet
    ADD CONSTRAINT devis_projet_pkey PRIMARY KEY (devis_projet_id);
 H   ALTER TABLE ONLY public.devis_projet DROP CONSTRAINT devis_projet_pkey;
       public            postgres    false    229            :           2606    24626 &   devis_ratio_item devis_ratio_item_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY public.devis_ratio_item
    ADD CONSTRAINT devis_ratio_item_pkey PRIMARY KEY (devis_ratio_item_id);
 P   ALTER TABLE ONLY public.devis_ratio_item DROP CONSTRAINT devis_ratio_item_pkey;
       public            postgres    false    225            &           2606    17584    devis_ratio devis_ratio_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.devis_ratio
    ADD CONSTRAINT devis_ratio_pkey PRIMARY KEY (devis_ratio_id);
 F   ALTER TABLE ONLY public.devis_ratio DROP CONSTRAINT devis_ratio_pkey;
       public            postgres    false    206            <           2606    24634     devis_tj_item devis_tj_item_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.devis_tj_item
    ADD CONSTRAINT devis_tj_item_pkey PRIMARY KEY (devis_tj_item_id);
 J   ALTER TABLE ONLY public.devis_tj_item DROP CONSTRAINT devis_tj_item_pkey;
       public            postgres    false    227            (           2606    17592    devistj devistj_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.devistj
    ADD CONSTRAINT devistj_pkey PRIMARY KEY (devis_tj_id);
 >   ALTER TABLE ONLY public.devistj DROP CONSTRAINT devistj_pkey;
       public            postgres    false    208            *           2606    17603    domaine domaine_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.domaine
    ADD CONSTRAINT domaine_pkey PRIMARY KEY (domaine_id);
 >   ALTER TABLE ONLY public.domaine DROP CONSTRAINT domaine_pkey;
       public            postgres    false    210            ,           2606    17608    echancier echancier_pkey 
   CONSTRAINT     s   ALTER TABLE ONLY public.echancier
    ADD CONSTRAINT echancier_pkey PRIMARY KEY (delivery_id, etape_echancier_id);
 B   ALTER TABLE ONLY public.echancier DROP CONSTRAINT echancier_pkey;
       public            postgres    false    211    211            J           2606    33035 $   etape_echancier etape_echancier_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.etape_echancier
    ADD CONSTRAINT etape_echancier_pkey PRIMARY KEY (etape_echancier_id);
 N   ALTER TABLE ONLY public.etape_echancier DROP CONSTRAINT etape_echancier_pkey;
       public            postgres    false    239            F           2606    33007    profil profil_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.profil
    ADD CONSTRAINT profil_pkey PRIMARY KEY (profil_id);
 <   ALTER TABLE ONLY public.profil DROP CONSTRAINT profil_pkey;
       public            postgres    false    236            .           2606    17635    projet projet_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.projet
    ADD CONSTRAINT projet_pkey PRIMARY KEY (projet_id);
 <   ALTER TABLE ONLY public.projet DROP CONSTRAINT projet_pkey;
       public            postgres    false    213            D           2606    32969    ratio ratio_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.ratio
    ADD CONSTRAINT ratio_pkey PRIMARY KEY (activite_id, delivery_id);
 :   ALTER TABLE ONLY public.ratio DROP CONSTRAINT ratio_pkey;
       public            postgres    false    234    234            H           2606    33017 
   tj tj_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.tj
    ADD CONSTRAINT tj_pkey PRIMARY KEY (delivery_id, profil_id);
 4   ALTER TABLE ONLY public.tj DROP CONSTRAINT tj_pkey;
       public            postgres    false    237    237            [           2606    32797 ,   devis_ratio_item fk2er9mq68ajdy05lh1ghae7j1e    FK CONSTRAINT     �   ALTER TABLE ONLY public.devis_ratio_item
    ADD CONSTRAINT fk2er9mq68ajdy05lh1ghae7j1e FOREIGN KEY (devis_item_id) REFERENCES public.devis_item(devis_item_id);
 V   ALTER TABLE ONLY public.devis_ratio_item DROP CONSTRAINT fk2er9mq68ajdy05lh1ghae7j1e;
       public          postgres    false    223    2872    225            \           2606    32802 )   devis_tj_item fk2vdb2nm7etk8adj8malw1l9bm    FK CONSTRAINT     �   ALTER TABLE ONLY public.devis_tj_item
    ADD CONSTRAINT fk2vdb2nm7etk8adj8malw1l9bm FOREIGN KEY (devis_item_id) REFERENCES public.devis_item(devis_item_id);
 S   ALTER TABLE ONLY public.devis_tj_item DROP CONSTRAINT fk2vdb2nm7etk8adj8malw1l9bm;
       public          postgres    false    2872    223    227            `           2606    33018    tj fk4s468j70xtp62kgwrjaaq2la    FK CONSTRAINT     �   ALTER TABLE ONLY public.tj
    ADD CONSTRAINT fk4s468j70xtp62kgwrjaaq2la FOREIGN KEY (delivery_id) REFERENCES public.delivery(delivery_id);
 G   ALTER TABLE ONLY public.tj DROP CONSTRAINT fk4s468j70xtp62kgwrjaaq2la;
       public          postgres    false    2880    231    237            V           2606    32818 !   devis fk54hrj0ctnw0fjdh9624c5csk7    FK CONSTRAINT     �   ALTER TABLE ONLY public.devis
    ADD CONSTRAINT fk54hrj0ctnw0fjdh9624c5csk7 FOREIGN KEY (devis_projet_id) REFERENCES public.devis_projet(devis_projet_id);
 K   ALTER TABLE ONLY public.devis DROP CONSTRAINT fk54hrj0ctnw0fjdh9624c5csk7;
       public          postgres    false    229    2878    215            ]           2606    32886 $   delivery fk7jt778b5q2quxtwtj8j6dgr1t    FK CONSTRAINT     �   ALTER TABLE ONLY public.delivery
    ADD CONSTRAINT fk7jt778b5q2quxtwtj8j6dgr1t FOREIGN KEY (projet_id) REFERENCES public.projet(projet_id);
 N   ALTER TABLE ONLY public.delivery DROP CONSTRAINT fk7jt778b5q2quxtwtj8j6dgr1t;
       public          postgres    false    2862    231    213            _           2606    32975 !   ratio fk8ca18a1g8g7ok86c8mah625t6    FK CONSTRAINT     �   ALTER TABLE ONLY public.ratio
    ADD CONSTRAINT fk8ca18a1g8g7ok86c8mah625t6 FOREIGN KEY (delivery_id) REFERENCES public.delivery(delivery_id);
 K   ALTER TABLE ONLY public.ratio DROP CONSTRAINT fk8ca18a1g8g7ok86c8mah625t6;
       public          postgres    false    231    2880    234            Z           2606    32792 &   devis_item fk9su6j637bvty755xj9w3i6ir5    FK CONSTRAINT     �   ALTER TABLE ONLY public.devis_item
    ADD CONSTRAINT fk9su6j637bvty755xj9w3i6ir5 FOREIGN KEY (devis_id) REFERENCES public.devis(devis_id);
 P   ALTER TABLE ONLY public.devis_item DROP CONSTRAINT fk9su6j637bvty755xj9w3i6ir5;
       public          postgres    false    215    223    2864            a           2606    33023    tj fkca1x9vrnseehe6kx4cegfelwa    FK CONSTRAINT     �   ALTER TABLE ONLY public.tj
    ADD CONSTRAINT fkca1x9vrnseehe6kx4cegfelwa FOREIGN KEY (profil_id) REFERENCES public.profil(profil_id);
 H   ALTER TABLE ONLY public.tj DROP CONSTRAINT fkca1x9vrnseehe6kx4cegfelwa;
       public          postgres    false    237    236    2886            M           2606    17661 ,   devis_complexite fkcpje2xn6pvrva0d0n61v71fnd    FK CONSTRAINT     �   ALTER TABLE ONLY public.devis_complexite
    ADD CONSTRAINT fkcpje2xn6pvrva0d0n61v71fnd FOREIGN KEY (domaine_id) REFERENCES public.domaine(domaine_id);
 V   ALTER TABLE ONLY public.devis_complexite DROP CONSTRAINT fkcpje2xn6pvrva0d0n61v71fnd;
       public          postgres    false    2858    204    210            L           2606    17651 &   complexite fkd8e02iuiot0yq244mgv8f4kq3    FK CONSTRAINT     �   ALTER TABLE ONLY public.complexite
    ADD CONSTRAINT fkd8e02iuiot0yq244mgv8f4kq3 FOREIGN KEY (domaine_id) REFERENCES public.domaine(domaine_id);
 P   ALTER TABLE ONLY public.complexite DROP CONSTRAINT fkd8e02iuiot0yq244mgv8f4kq3;
       public          postgres    false    202    2858    210            T           2606    33036 %   echancier fkem1uc1y7wu56grj6h3w6dqtc0    FK CONSTRAINT     �   ALTER TABLE ONLY public.echancier
    ADD CONSTRAINT fkem1uc1y7wu56grj6h3w6dqtc0 FOREIGN KEY (etape_echancier_id) REFERENCES public.etape_echancier(etape_echancier_id);
 O   ALTER TABLE ONLY public.echancier DROP CONSTRAINT fkem1uc1y7wu56grj6h3w6dqtc0;
       public          postgres    false    211    239    2890            X           2606    24640 +   devis_echancier fkepmsjb1jrb6420vkj1bqmamjp    FK CONSTRAINT     �   ALTER TABLE ONLY public.devis_echancier
    ADD CONSTRAINT fkepmsjb1jrb6420vkj1bqmamjp FOREIGN KEY (devis_echancier_item_id) REFERENCES public.devis_echancier_item(devis_echancier_item_id);
 U   ALTER TABLE ONLY public.devis_echancier DROP CONSTRAINT fkepmsjb1jrb6420vkj1bqmamjp;
       public          postgres    false    219    221    2870            P           2606    17681 #   devistj fkh0hrkdoi9ya1qda31tdproqjs    FK CONSTRAINT     �   ALTER TABLE ONLY public.devistj
    ADD CONSTRAINT fkh0hrkdoi9ya1qda31tdproqjs FOREIGN KEY (client_id) REFERENCES public.client(client_id);
 M   ALTER TABLE ONLY public.devistj DROP CONSTRAINT fkh0hrkdoi9ya1qda31tdproqjs;
       public          postgres    false    208    200    2848            W           2606    32782 1   devis_complexite_item fkhbavfjvkr0ll0a22geyfakmag    FK CONSTRAINT     �   ALTER TABLE ONLY public.devis_complexite_item
    ADD CONSTRAINT fkhbavfjvkr0ll0a22geyfakmag FOREIGN KEY (devis_item_id) REFERENCES public.devis_item(devis_item_id);
 [   ALTER TABLE ONLY public.devis_complexite_item DROP CONSTRAINT fkhbavfjvkr0ll0a22geyfakmag;
       public          postgres    false    217    2872    223            K           2606    17646 %   categorie fkiao0s1vsjym1uqvhe0w9syef9    FK CONSTRAINT     �   ALTER TABLE ONLY public.categorie
    ADD CONSTRAINT fkiao0s1vsjym1uqvhe0w9syef9 FOREIGN KEY (projet_id) REFERENCES public.projet(projet_id);
 O   ALTER TABLE ONLY public.categorie DROP CONSTRAINT fkiao0s1vsjym1uqvhe0w9syef9;
       public          postgres    false    198    2862    213            Y           2606    32787 0   devis_echancier_item fkibtvglmtjxkuv2dv2vdr4dejv    FK CONSTRAINT     �   ALTER TABLE ONLY public.devis_echancier_item
    ADD CONSTRAINT fkibtvglmtjxkuv2dv2vdr4dejv FOREIGN KEY (devis_item_id) REFERENCES public.devis_item(devis_item_id);
 Z   ALTER TABLE ONLY public.devis_echancier_item DROP CONSTRAINT fkibtvglmtjxkuv2dv2vdr4dejv;
       public          postgres    false    223    2872    221            N           2606    24635 ,   devis_complexite fkix7mvxalfy3qivmunp3vw7lhg    FK CONSTRAINT     �   ALTER TABLE ONLY public.devis_complexite
    ADD CONSTRAINT fkix7mvxalfy3qivmunp3vw7lhg FOREIGN KEY (devis_complexite_item_id) REFERENCES public.devis_complexite_item(devis_complexite_item_id);
 V   ALTER TABLE ONLY public.devis_complexite DROP CONSTRAINT fkix7mvxalfy3qivmunp3vw7lhg;
       public          postgres    false    2866    204    217            O           2606    24645 '   devis_ratio fkk3a27ueh8qimnxpcg2ybn7n56    FK CONSTRAINT     �   ALTER TABLE ONLY public.devis_ratio
    ADD CONSTRAINT fkk3a27ueh8qimnxpcg2ybn7n56 FOREIGN KEY (devis_retio_item_id) REFERENCES public.devis_ratio_item(devis_ratio_item_id);
 Q   ALTER TABLE ONLY public.devis_ratio DROP CONSTRAINT fkk3a27ueh8qimnxpcg2ybn7n56;
       public          postgres    false    206    225    2874            ^           2606    32970 !   ratio fkl4x2mot78tt3foxicmy6b68n7    FK CONSTRAINT     �   ALTER TABLE ONLY public.ratio
    ADD CONSTRAINT fkl4x2mot78tt3foxicmy6b68n7 FOREIGN KEY (activite_id) REFERENCES public.activite(activite_id);
 K   ALTER TABLE ONLY public.ratio DROP CONSTRAINT fkl4x2mot78tt3foxicmy6b68n7;
       public          postgres    false    234    2882    233            S           2606    32891 %   echancier fknd3uf8fjbp7botlnohx9oxxvq    FK CONSTRAINT     �   ALTER TABLE ONLY public.echancier
    ADD CONSTRAINT fknd3uf8fjbp7botlnohx9oxxvq FOREIGN KEY (delivery_id) REFERENCES public.delivery(delivery_id);
 O   ALTER TABLE ONLY public.echancier DROP CONSTRAINT fknd3uf8fjbp7botlnohx9oxxvq;
       public          postgres    false    211    231    2880            R           2606    17691 "   domaine fknsnfpom6f1exjxu3k5oyeihc    FK CONSTRAINT     �   ALTER TABLE ONLY public.domaine
    ADD CONSTRAINT fknsnfpom6f1exjxu3k5oyeihc FOREIGN KEY (categorie_id) REFERENCES public.categorie(categorie_id);
 L   ALTER TABLE ONLY public.domaine DROP CONSTRAINT fknsnfpom6f1exjxu3k5oyeihc;
       public          postgres    false    198    2846    210            U           2606    17706 "   projet fkocfbp78fn7nchnfacacpyvhp8    FK CONSTRAINT     �   ALTER TABLE ONLY public.projet
    ADD CONSTRAINT fkocfbp78fn7nchnfacacpyvhp8 FOREIGN KEY (client_id) REFERENCES public.client(client_id);
 L   ALTER TABLE ONLY public.projet DROP CONSTRAINT fkocfbp78fn7nchnfacacpyvhp8;
       public          postgres    false    213    2848    200            Q           2606    24650 #   devistj fkqe8htgqhjl3o57hlxi0006ndx    FK CONSTRAINT     �   ALTER TABLE ONLY public.devistj
    ADD CONSTRAINT fkqe8htgqhjl3o57hlxi0006ndx FOREIGN KEY (devis_tj_item_id) REFERENCES public.devis_tj_item(devis_tj_item_id);
 M   ALTER TABLE ONLY public.devistj DROP CONSTRAINT fkqe8htgqhjl3o57hlxi0006ndx;
       public          postgres    false    227    2876    208            �   �  x�MRK��0]S�Ю�6��:]vc A{Rtэ s<�ʒ�O��es_���L�E=���Π)6�H;(�A!Z':��xr���\�VR��nड़?�o�b���Ug�-4Oe2J��uN���'k���=/�SJ���A^ϐ;��ʀ<>D�&��S*o?E��T���=��9��-������a���N7�:�~*�`U���::�<%n��-:��c��f�`[�`��$͇�g�	��5��Bj�&�GX/�ɔG�f��q�!��7M�, '>#�*H�C��w��R9�
�r�R����a�Rob"��ӥ%��	�Y���Y&���7���fwi��M��������T�;D�f ����
Zmpi$
�A�)��2N���yv���F���'��&��1(����ެ��Zs�R�~ݙZ�cr�|�>|�B,{���ž���KN�}u�@�tԙ�Kf������M�      �   �   x���=
�0��>��.&пtnj��:g1�(��6��;��X�бCz�{h+:CH0 h�\�H
:�m�Q��ՈNk��;�K��8�K
.���9��$K<�}�����^���}���P�[��b�����lԵ�$�%3���&�|L����BD"��xb=0�)��sT�      �   �   x�=��j�@D��Wl���w:�T��M&i�\���9��$}�~,3S3<��kw��E�%��Y���-0���#:�3^ӺL���.����v�^C��>�[g��V���pOq�ې��&@d�i��"b�@�%��h	�
����=��i����[�2j�U�|�Q��R���yS�Ʋc�7C������`�L]Ҧ�&ؾ�V�����(�?��Jb      �   <  x�u�An�0E��)8A���:m�.RUj�ˆ�+!;*�����:6I�]���{��o ���[��VI#�����LV��D�������u�~�u8�~j��Tì�i��K<�ZwZ)�Bw_G���O�����q�.�Vc_�j�9�0�f99<����3$���d�3A-�pC������Ďl�ise��!�t��	f �������|�d�c��/��E�JB���v�)ei�ᑰ <"��(���)�[!��
Y�
Q�*���T��]UP�.���}Ha�Nl���G�m��+�M.�ٕ.i6��?� �      �   {   x�3���O9�"'U!�2H��)�)$%g��*��'�s�A�bN3.#N�����ļ�T����<�:N���1g@Q*P�$3?O!E=1/1��8UA_��ʒҔT�PfnAbrI1�#D
�'F��� ��+�      �   p   x�}�1�@�^qb9�y�_�2.\G���)���b5F�:2�ݴ2�"U+���~<�G�[×XO�.�Z���(*� t��5�E�	��Wp��g��5�wa�7�,      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �   &  x��ֽj�0��Y�
o�J��(�J��nYB�B�`���Q�7ִP
�~�@>�Jav��]^��볮���Լ�]�Nm�Ǫ/U3t���k�}?T]x�޿���y]�ޢ�q���(����˺,gO��3�MS�����%�b���Kp���3��95o��y��O̓u$�G�}�C`�؇�>�!��}�C`
�P؇ҿ؇�>����}(�Ca�0؇�>����}6؛�>�a��}8��a�p؇�}v؛�>���}d�G�}d�G�}d���.��N^      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �   �   x�]�KN�@�מS� (�G�5%A��C�Xu3LL4e��l�۰�9r1*�cg���>��>hτ5a�8/�_l�>$��;�5�0j���9��T�MCl�Kn����H
X=oI.��:��UT�=�����=�΋�7��qa�%l��4�bi9w����8���dcS��8[� ��ˬ���5����Vl��37�j����5%���>P�S\�7�Wfl
��'��f      �       x�3�4�46�2��4Q��F\1z\\\ 2��         0   x�3�t���M�KI����,+J�,���24�K��LI,��b���� ��         0   x�3�t�K̩,.I�2�t9��,5'�� ���˘ӵ� ���+F��� ��      �   -  x���9n�0E��)عR�%��J�"(�e���mO�p�E���BZ��&������(yBC�q���E��UҞ�0�CM����N�}���� ��7^��]���X��hr�d��f�PM�� �t�5$��4��$��QI�^����M���>��8~V�?o�^��R�Q�в��Фd{)�@�ME�,_װYeS��������W-O|/��y�-��|��T���.8�=�w���͸�Zf�t�˥�~^^X%{�*x\/��2ٔ�`�$dߣ:��z�oiUtB����� >�Ҡ�          ]   x�U���0k4g�p�]�������P��>w��~ڤ��"A��1�FD���x�����k����xD�e��r(�|�H�3�٢�:�	?             x�3�4�450�2�4� �Ɯf@:F��� ;��     