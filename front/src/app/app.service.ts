import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})



export class AppService {

  constructor(private http: HttpClient) { }

  rootURL = 'http://localhost:8080/api';
  project: any ;
  devisProjetId;

  getClient(){
    return this.http.get(this.rootURL+'/client');
  }
  
  getClientById(id: number){
    const url = `${this.rootURL+'/client'}/${id}`;
    return this.http.get(url);
  }

  getProjectByClientId(id: number){
    const url = `${this.rootURL+'/projet/client'}/${id}`;
    return this.http.get(url);
  }

  getCategorieByProjectId(id: number){
    const url = `${this.rootURL+'/categorie/project'}/${id}`;
    return this.http.get(url);
  }

  getDomainesByCategorieId(id: number){
    const url = `${this.rootURL+'/domaine/categorie'}/${id}`;
    return this.http.get(url);
  }

  getDomaineById(id: number){
    const url = `${this.rootURL+'/domaine'}/${id}`;
    return this.http.get(url);
  }

  getComplexiteByDomaine(id: number){
    const url = `${this.rootURL+'/complexite/domaine'}/${id}`;
    return this.http.get(url);
  }

  getProjetById(id: number){
    const url = `${this.rootURL+'/projet'}/${id}`;
    return  this.http.get(url);
  }

  getDeliveryById(id:number){
    const url = `${this.rootURL+'/delivery'}/${id}`;
    return  this.http.get(url);
  }


  async createDevisProjet(p: number){
    const url = `${this.rootURL+'/devisProjet'}`;
    await (await this.getProjetById(p)).subscribe(project => {
      this.project = project;
    });
    return  await this.http.post<any>(url, {
      nomProjet: this.project.nomProjet,
      commentaire: this.project.commentaire,
      numContrat: this.project.numContrat
    });
  }

  
  async createDevis(p: number) {
    const url = `${this.rootURL+'/devis'}`;
    return await this.http.post<any>(url, {
      libelle: "devis 1",
      dataCreation: new Date(),
      dateValidation: new Date(),
      devisProjet:{
        devisProjetId: p
      }
    });
  }

  getAllComplexite(){
    const url = `${this.rootURL+'/complexite'}`;
    return this.http.get(url);
  }

  getDeliveryByProjectId(id: number){
    const url = `${this.rootURL+'/delivery/projet'}/${id}`;
    return this.http.get(url);
  }

  getRatioByDeliveryId(id: number){
    const url = `${this.rootURL+'/ratio/delivery'}/${id}`;
    return this.http.get(url);
  }

  getActiviteByActiviteId(id: number){
    const url = `${this.rootURL+'/activite'}/${id}`;
    return this.http.get(url);
  }

  getTjByDeliveryId(id: number){
    const url = `${this.rootURL+'/tj/delivery'}/${id}`;
    return this.http.get(url);
  }

  getProfilByProfilId(id: number){
    const url = `${this.rootURL+'/profil'}/${id}`;
    return this.http.get(url);
  }

  getEchancierByDeliveryId(id: number){
    const url = `${this.rootURL+'/echancier/delivery'}/${id}`;
    return this.http.get(url);
  }

  getEtapeByEtapeId(id: number){
    const url = `${this.rootURL+'/etapeEchancier'}/${id}`;
    return this.http.get(url);
  }


}
