import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageChiffrageComponent } from './page-chiffrage.component';

describe('PageChiffrageComponent', () => {
  let component: PageChiffrageComponent;
  let fixture: ComponentFixture<PageChiffrageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageChiffrageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageChiffrageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
