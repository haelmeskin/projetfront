import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { AppService } from '../app.service';



@Component({
  selector: 'app-page-chiffrage',
  templateUrl: './page-chiffrage.component.html',
  styleUrls: ['./page-chiffrage.component.css']
})
export class PageChiffrageComponent implements OnInit {

  constructor(private appService: AppService,private route: ActivatedRoute, private router: Router) { }
  client : string
  projet : string
  projetId: number;
  categories : any[] = [] 
  domaines : any[] = []
  domaineNames: any[] = []
  complexite: any[] = []
  tableau: any[] = []
  count: any[] = []
  nom: string
  private fieldArray: Array<any> = [];
  private newAttribute: any = {};
  longeur: number = 0 ;
  row: Array<number>;
  AllComplexite: number[] = []
  AllComplexiteValue: number[] = []

  //information of rows
  num: any[] = [];
  evolution : any[] = [];
  function: any[] = [];
  complexites: number[] = [];
  countRow: number = 0;
  countColumn: number = 0;
  total: number [] = [];
  index: number = -1;

  ngOnInit() {
      this.route.queryParamMap.subscribe(params => {
        this.client = params.get('nomClient');
        this.projet = params.get('nomProjet');
        this.categories = params.getAll('listCategorie');
        this.domaines = params.getAll('listDomaine');
        this.projetId = Number(params.get('projetId'));

        //getAllComplexite
        this.appService.getAllComplexite().subscribe((AllComplexite: any[]) => {
          this.AllComplexite =  this.AllComplexite.concat(AllComplexite);
        });  
       
        //get domaine by name
        for (let index = 0; index < this.domaines.length; index++) {
          this.appService.getDomaineById(this.domaines[index]).subscribe((domaineNames: any[]) => {
            this.domaineNames =  this.domaineNames.concat(domaineNames['nom']);
          });          
        }

        //get complexite by domaine
        for (let index = 0; index < this.domaines.length; index++) {
          this.appService.getComplexiteByDomaine(this.domaines[index]).subscribe((complexite: any[]) => {
            this.count[index] = complexite.length;  
            this.complexite = complexite;
            this.tableau =  this.tableau.concat(complexite)
          });
               
        }
      });
  }


  //ajouter une ligne
  addItem(){
    
    this.index++;

    
    this.longeur = this.longeur + 1
    this.row = new Array(this.longeur)
    for (let index = 0; index < this.row.length; index++) {
      this.row[index] = index + 1;
    }

    //nombre de ligne
    this.countRow++;
    
    for (let index = 0; index < this.AllComplexite.length; index++) {
      //mettre le valeurs saisit pour chaque complexités dans une tables
      //initialiser la table par des zéro
      this.complexites[index] = 0;
      //mettre les complexites dans une tables
      //initialiser la table par des zéro
      this.AllComplexiteValue[index] = 0
    }
    //add complexites to tables
  }

  

  //every time, I enter a value I call this function onKeyXXX
  //à chaque fois, j'entre une valeur J'appelle cette fonction onKeyXXX

  //Pour le numero de la demande
  onKeyNum(event: any) { // without type info    
    this.num[this.countRow] = event.target.value

  }

  //Pour le champ évolution
  onKeyE(event: any) { // without type info
    this.evolution[this.countRow] = event.target.value ;
  }

  //Pour le champ fonction
  onKeyF(event: any) { // without type info
    this.function[this.countRow] = event.target.value ;
  }

  //Pour les champs complexités
  onKeyC(event: any, column, id) { // without type info

    //si je supprime une valeur (value == empty) 
    if(event.target.value=="")
      this.complexites[id] = Number(0);
    else{
    //si j'ajoute une valeur (value == 40) 
      this.complexites[id] = event.target.value ;
    }
    
    this.AllComplexiteValue[id] = column


    console.log(this.AllComplexiteValue)
    
    //Le calcul du total
    this.total[this.index] = 0;
    
    for (let index = 0; index < this.AllComplexite.length; index++) {
      this.total[this.index] = this.total[this.index] + (this.complexites[index] *  this.AllComplexiteValue[index])
    }
  }

  //chaque ligne est un item devis (evolution, fonction)
  //devis complexite contient les complexites
  //complexite_tem = liste des devis complexite

  //on Submit
  onSave(){
    console.log("Num = ", this.num);
    console.log("Ev = ", this.evolution);
    console.log("Fu = ", this.function);
    console.log("Com = ", this.complexites);

    this.router.navigate(['/pageDelivery'],
    {queryParams: 
      {
        num: this.num,
        evolution: this.evolution, 
        function: this.function,
        complexites: this.complexites,
        projetId: this.projetId,
        totals: this.total,
        client: this.client,
        projet: this.projet
        }
      }).then(()=>{
        window.location.reload();
      });

  }

}
