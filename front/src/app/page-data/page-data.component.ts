import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import {FormControl, FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Router } from '@angular/router';



@Component({
  selector: 'app-page-data',
  templateUrl: './page-data.component.html',
  styleUrls: ['./page-data.component.css'],

})

export class PageDataComponent implements OnInit {
  constructor(private appService: AppService, private formBuilder: FormBuilder,  private router: Router) { }

  users: any[] = [];
  projects: any[] = [];
  categories: any[]= [];
  domaines: any[]= [];
  isClose: boolean;
  valueSelected: string;
  selectedCat = new FormControl();
  selectedDom = new FormControl();
  myGroup: FormGroup;
  cat = [];
  dom = [];
  projetId: number
  clientId: number
  data;


  ngOnInit() {
    this.myGroup = this.formBuilder.group({
      nom: ['', Validators.required],
      projet: ['', Validators.required],
      dom: [[], Validators.required],
      cat: [[], Validators.required]
    });

    this.appService.getClient().subscribe((users: any[]) => {
      this.users = users;
    });
    
  }

  //récupérer le nom du client
  changeClient(selectedClient) {
    this.clientId = selectedClient
    this.appService.getProjectByClientId(selectedClient).subscribe((projects: any[]) => {
      this.projects = projects;
      console.log(projects)
    });
  }

  //récupérer le nom du projet selon le client choisit
  changeProject(selectedProject){
    this.projetId = selectedProject;
    this.appService.getCategorieByProjectId(selectedProject).subscribe((categories: any[]) => {
      this.categories = categories;
    });
  }

  //récupérer les categories selon le projet choisit
  changeCategorie(selectedCategories){
    this.isClose = false;
    if(!selectedCategories) {
      this.isClose = true;
      this.domaines = []
      this.myGroup.value.cat = this.selectedCat.value
      for(let i=0; i<this.selectedCat.value.length;i++){
        console.log(this.selectedCat.value[i])
        this.appService.getDomainesByCategorieId(this.selectedCat.value[i]).subscribe((domaines: any[]) => {
          this.domaines = this.domaines.concat(domaines);
        });
      }
    }
  }

  //récupérer les domaines selon les catégories choisit
  changeDomaine(selectedDomaines){
    this.isClose = false;
    if(!selectedDomaines) {
      this.isClose = true;
      this.myGroup.value.dom = this.selectedDom.value
      for(let i=0; i<this.selectedDom.value.length;i++)
        console.log(this.selectedDom.value[i])
    }
  }

  //create devis
  //create devis project


  async onSubmit() {

    await (await this.appService.createDevisProjet(this.projetId)).subscribe(data => {
      this.data = data;
    });

    await this.appService.createDevis(this.data.devisProjetId);

    
    this.router.navigate(['/pageChiffrage'],
    {queryParams: 
      {
        nomClient: this.myGroup.value.nom, 
        nomProjet: this.myGroup.value.projet,
        listCategorie: this.myGroup.value.cat,
        listDomaine: this.myGroup.value.dom,
        projetId: this.projetId 
      }, 
     
    }
).then(()=>{
  window.location.reload();
});

  }

 
    //devis item

}
