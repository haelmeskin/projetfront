import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { AppService } from '../app.service';

 

@Component({
  selector: 'app-page-delivery',
  templateUrl: './page-delivery.component.html',
  styleUrls: ['./page-delivery.component.css']
})
export class PageDeliveryComponent implements OnInit {

  constructor(private appService: AppService,private route: ActivatedRoute, private router: Router) { }

  client;
  projet;
  num: any[] = [];
  function: any[] = [];
  complexites: any[] = [];
  evolution : any[] = [];
  delivery: any[] = [];
  totalsString: any[] = [];
  totals: number[] = [];
  projetId: number;
  ratios: any[] = [];
  pourcentages: any[] = [];
  activite: any[] = [];
  activiteId;
  index: number = 0;
  indexp: number = 0;
  calculs = [];
  profils: any[] = [];
  totalsRatio: any[] = [];
  totalTotals: number = 0;
  selectedProfil: any[] = [];
  indexProfil: number = 0;
  ListCout: any[] = []
  cout: number;
  calculTj: number[] = []
  totalCout: number = 0;
  totalTj: number = 0;
  checked = false;
  modelDelivery: string;
  
  

  ngOnInit() {
    this.route.queryParamMap.subscribe(params => {
      this.num = params.getAll('num');
      this.evolution = params.getAll('evolution');
      this.function = params.getAll('function');
      this.complexites = params.getAll('complexites');
      this.projetId = Number(params.get('projetId'));
      this.totalsString = (params.getAll('totals'));
      this.client = params.get('client');
      this.projet = params.get('projet');
      for (let index = 0; index < this.totalsString.length; index++) {
        this.totals[index] = Number(this.totalsString[index]);
      }
    });
  

    //get modele de delivery par projet
    this.appService.getDeliveryByProjectId(this.projetId).subscribe((delivery: any[]) => {
      this.delivery = delivery;
    });
  }

  //appeler lors du choix du modele de delivery
  changeModele(selectedModele){
      this.activite = []
      this.pourcentages = []
      this.indexp = 0;
      this.modelDelivery = selectedModele;

       //get profils
      this.appService.getTjByDeliveryId(selectedModele).subscribe((tjs: any[]) => {
        for (let index = 0; index < Object.entries(tjs).length; index++) {

          //pour chaque profil, je récupère son cout et je stock tout dans une table ListeCout
          this.ListCout = this.ListCout.concat((Object.entries(tjs)[index])[1].cout )

          this.appService.getProfilByProfilId((Object.entries(tjs)[index])[1].id.profilId).subscribe((profils: any[]) => {
            this.profils = this.profils.concat(profils);
          });
        }
      });
      
      
      //1- get ratio (idActivite, idDelivery, ....)
      this.appService.getRatioByDeliveryId(selectedModele).subscribe((ratios: any[]) => {
       
        for (let index = 0; index < Object.entries(ratios).length; index++) {

          //pour chaque ratio, je récupere le pourcentage et je stock tout dans une table pourcentages
          this.pourcentages[this.indexp] = (Object.entries(ratios)[index])[1].pourcentage
          this.indexp++;

          this.appService.getActiviteByActiviteId((Object.entries(ratios)[index])[1].id.activiteId).subscribe((activite: any[]) => {
            this.activite = this.activite.concat(activite);
          });
        }

        //matrice [ratio][ligne] : pour garder tout les valeurs calculer de chaque ligne, matrice est un bon choix
        this.calculs = new Array(Object.entries(ratios).length);
        for (let index = 0; index < Object.entries(ratios).length; index++) {
          this.calculs[index] = new Array(this.totals.length)
        }


        for (let index = 0; index < Object.entries(ratios).length; index++) {
          for (let index1 = 0; index1 < this.totals.length; index1++) {
            this.calculs[index][index1] = this.totals[index1] * ((Object.entries(ratios)[index])[1].pourcentage / 100);
            //total de chaque ligne
            this.totalsRatio[index1] = 0;
          }
        }

        //le calcul du total
        for (let index1 = 0; index1 < this.totals.length; index1++) {
          for (let index = 0; index < Object.entries(ratios).length; index++) {
            this.totalsRatio[index1] = this.totalsRatio[index1] + this.calculs[index][index1] ;
          }
          this.totalsRatio[index1] = this.totalsRatio[index1] + this.totals[index1]
          this.totalTotals = this.totalTotals + this.totalsRatio[index1];
        }
      });
  }
 
//fonction appeler dans chaque changement de profil
  changeProfil(selectedP){
    console.log(selectedP)
    if(this.checked == false){
      this.totalTj  = 0
      this.totalCout = 0
      for (let index1 = 0; index1 < this.totals.length; index1++) {
        this.calculTj[index1] = 0
      }
      
      this.cout = this.ListCout[selectedP - 1]
      this.selectedProfil[this.indexProfil] = this.cout;
      
      //calcul TJ
      this.calculTj[this.indexProfil] = this.totalsRatio[this.indexProfil] * this.selectedProfil[this.indexProfil]
    

      //calcul total Cout
      this.totalCout = this.totalCout + this.cout;

      //calcul Total TJ
      this.totalTj = this.totalTj + this.calculTj[this.indexProfil]
      this.indexProfil++;
    }
    else{
      this.selectedProfil[this.indexProfil] = 0
      //this.indexProfil++;
    }
  }

  //appeler si je coche la case du tarif unique
  changeAll(event){
    // si non => rien
    if(this.checked == false)
      console.log(this.checked)
    //si oui => mettre tout les valeurs à zérp
    else{
      this.totalTj  = 0
      this.totalCout = 0
      for (let index1 = 0; index1 < this.totals.length; index1++) {
        this.calculTj[index1] = 0
      }

    }
  }


//appeler lors de la saisi du cout (tarif unique)
  onCoutChange(event: any){
    this.totalCout = event.target.value
    console.log(this.totalCout)

    for (let index1 = 0; index1 < this.totals.length; index1++) {
      this.calculTj[index1] = this.totalsRatio[index1] * this.totalCout
      this.totalTj = this.totalTj + this.calculTj[index1]
    }
  }

//create devis-ratio et devis-ratio-item
//create devis-tj et devis-tj-item

//onSubmit
  onSave(){
    this.router.navigate(['/pageEchancier'],
    {queryParams: 
      {
        total: this.totalTotals,
        projetId: this.projetId,
        client: this.client ,
        projet: this.projet,
        model:  this.modelDelivery
      }
      });

  }

}
