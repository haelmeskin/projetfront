import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageEchancierComponent } from './page-echancier.component';

describe('PageEchancierComponent', () => {
  let component: PageEchancierComponent;
  let fixture: ComponentFixture<PageEchancierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageEchancierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageEchancierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
