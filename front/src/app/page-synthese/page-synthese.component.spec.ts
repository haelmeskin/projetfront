import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageSyntheseComponent } from './page-synthese.component';

describe('PageSyntheseComponent', () => {
  let component: PageSyntheseComponent;
  let fixture: ComponentFixture<PageSyntheseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageSyntheseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageSyntheseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
