import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { AppService } from '../app.service';

@Component({
  selector: 'app-page-synthese',
  templateUrl: './page-synthese.component.html',
  styleUrls: ['./page-synthese.component.css']
})
export class PageSyntheseComponent implements OnInit {

  constructor(private appService: AppService,private route: ActivatedRoute, private router: Router) { }

  client;
  projet;
  NomClient;
  NomProjet;
  model;
  total: number;
  etapes;
  Montant;
  dates;
  delivery;


   ngOnInit() {
    this.route.queryParamMap.subscribe(params => {
      this.total = Number(params.get('total'));
      this.client = Number(params.get('client'));
      this.projet = params.get('projet');
      this.etapes = params.getAll('etapes');
      this.Montant = params.getAll('montant');
      this.dates = params.getAll('dates');
      this.model = params.get('delivery');
    });


    //get client by id
    this.appService.getClientById(this.client).subscribe(Client => {
      this.NomClient =  Client['nom'];
    }); 

    //get Projet by id
   ( this.appService.getProjetById(this.projet)).subscribe(Projet => {
      this.NomProjet =  Projet['nomProjet'];
    }); 


  }

  onSave(){
    this.router.navigate(['/pageVisualiser'])
  }

}
