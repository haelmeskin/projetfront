import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageVisualiserComponent } from './page-visualiser.component';

describe('PageVisualiserComponent', () => {
  let component: PageVisualiserComponent;
  let fixture: ComponentFixture<PageVisualiserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageVisualiserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageVisualiserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
